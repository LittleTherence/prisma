const { PrismaClient } = require('@prisma/client')
const prisma = new PrismaClient()

async function main() {
	const categories = await prisma.category.findMany({})
  console.log(categories)

  const events = await prisma.event.findMany({})
  console.log(events)

  const oneEvent = await prisma.event.findUnique({
    where: {
      id: 3
    },
    select: {
      title: true,
      categories: true
    }
  })
  console.log(oneEvent)

  const addCategory = await prisma.category.create({
    data: {
      name: "Art de rue",
    }
  })
}

main()
  .then(async() => {
    await prisma.$disconnect()
  })
  .catch(async(e) => {
    console.error(e)
    await prisma.$disconnect()
    process.exit(1)
  })