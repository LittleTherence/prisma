const { PrismaClient } = require('@prisma/client')
const prisma = new PrismaClient()
const { faker } = require('@faker-js/faker')

async function main() {
	// Exécution des requêtes via le client Prisma

    const deleteCategories = await prisma.category.deleteMany({})
    const deletedEvents = await prisma.event.deleteMany({})

    for(let i=1; i<= 10; i++) {
        await prisma.category.create({
            data: {
                name: faker.lorem.word(),
            }
        })
    }

    for (let index = 0; index < 10; index++) {
        await prisma.event.create({
            data: {
                title: faker.lorem.sentence(1),
                description: faker.lorem.lines(2),
                startedAt: faker.datatype.datetime(),
                endedAt: faker.datatype.datetime(),
                createdAt: faker.datatype.datetime(),
                isPremium: faker.datatype.boolean(),
                // categories: faker.helpers.arrayElement(await prisma.event.findMany({ data: {id: true}}))
            }
        })
    }

    await prisma.CategoriesOnEvents.create({
        data: {
            categoryId: 61,
            eventId: 61
        }
    })
}

main()
	.then(async () => {
		await prisma.$disconnect()
	})
	.catch(async (e) => {
		console.error(e)
		await prisma.$disconnect()
		process.exit(1)
	})