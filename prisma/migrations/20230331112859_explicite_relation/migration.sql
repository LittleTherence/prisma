/*
  Warnings:

  - You are about to drop the `_categorytoevent` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE `_categorytoevent` DROP FOREIGN KEY `_CategoryToEvent_A_fkey`;

-- DropForeignKey
ALTER TABLE `_categorytoevent` DROP FOREIGN KEY `_CategoryToEvent_B_fkey`;

-- DropTable
DROP TABLE `_categorytoevent`;

-- CreateTable
CREATE TABLE `CategoriesOnEvents` (
    `categoryId` INTEGER NOT NULL,
    `eventId` INTEGER NOT NULL,

    PRIMARY KEY (`categoryId`, `eventId`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB;

-- AddForeignKey
ALTER TABLE `CategoriesOnEvents` ADD CONSTRAINT `CategoriesOnEvents_categoryId_fkey` FOREIGN KEY (`categoryId`) REFERENCES `Category`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `CategoriesOnEvents` ADD CONSTRAINT `CategoriesOnEvents_eventId_fkey` FOREIGN KEY (`eventId`) REFERENCES `Event`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
